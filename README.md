# Formulários

Este projeto contém campos de formulário e formulários comuns para os sites da Weecom, já com alguns plugins úteis. Utilize-o sabiamente. ;)

## To do

* Verificar se dá pra implementar algo de custom validate também: http://tableless.com.br/html5/?chapter=8
* Colocar uma validação de CPF baseada no algoritmo de CPF.

## [Changelog](CHANGELOG.md)

Sempre que alterar este projeto, altere também o changelog.
