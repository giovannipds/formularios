<?php

if ($_SERVER['SERVER_NAME'] === 'server') {
	define('IS_LOCALHOST', true);
} else {
	define('IS_LOCALHOST', false);
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Padrões de Formulários &lt; Weecom</title>
		<meta name="description" content="Padrões de formulários e padrões de campos de formulários da Weecom, centralizados, para você copiar e colar. Se precisar de algum outro tipo de campo ou de formulário específico, prefira melhorá-lo nesse projeto, seja para criar uma versão diferente de um campo, melhorá-lo, corrigí-lo, etc.">
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/fancyselect.css">
		<link rel="stylesheet" href="css/main.css">
	</head>
	<body>
		<main>
			<section>
				<h1>Instruções</h1>
				<ul>
					<li>As partes do arquivo que você deve copiar e colar tem um <code>enter</code> antes e depois e com o comentário <code>&lt;!-- Para copiar e colar --&gt;</code> acima.</li>
					<li>Use o bom senso e o seu conhecimento para adicionar, retirar ou modificar o que for preciso de acordo com o seu projeto.</li>
					<li>Os formulários e campos padrões já vem com uma <code>div</code> separando cada campo, com a classe <code>.field-box</code> (caixa-do-campo) + <code>.nome-do-campo</code> para facilitar a estilização, além de uma classe <code>.field</code> (campo) no próprio campo.</li>
					<li>Se o seu campo deixar de ser obrigatório, verifique a marcação (<code>required</code> e <code>title</code>) além do JavaScript.</li>
					<li>Se for copiar o formulário padrão dê uma olhada nas dependências e arquivos de JavaScripts no final da página e no arquivo externo adicional <code>main.js</code>.</li>
				</ul>
			</section>
			<section>
				<h1>Padrões de Formulários</h1>
				<section id="formularios-com-rotulos">
					<h2>Formulários com Rótulos</h2>

					<!-- Para copiar e colar -->
					<form action="#formularios-com-rotulos" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>
							<div class="field-box nome">
								<label for="nome">Nome *</label><br>
								<input class="field" id="nome" maxlength="1055" name="nome" pattern="^[^0-9]{2,1055}$" required="required" title="Campo de texto obrigatório e de 2 a 1055 caracteres." type="text" />
							</div>
							<div class="field-box email">
								<label for="email">E-mail *</label><br>
								<input class="field" id="email" name="email" pattern="^[A-Za-z0-9\.-_@]{6,345}$" required="required" type="email" title="Campo de e-mail obrigatório, alfanumérico, com ou sem caracteres especiais como &quot;.&quot;, &quot;-&quot; ou &quot;_&quot; e de 6 a 345 caracteres." />
							</div>
							<div class="field-box telefone_nacional">
								<label for="telefone_nacional">Telefone *</label><br>
								<input class="field" id="telefone_nacional" name="telefone_nacional" pattern="^\d{2} \d{4}\-\d{4}[0-9_]?$" required="required" title="Campo de telefone obrigatório, somente com digítos, DDD e com o nono dígito opcional, como por exemplo: 99 9999-99999." type="tel" />
							</div>
							<div class="field-box mensagem">
								<label for="mensagem">Mensagem *</label><br>
								<textarea class="field autosize" id="mensagem" name="mensagem" required="required" title="Campo de texto obrigatório."></textarea>
							</div>
							<input type="submit" value="Enviar">
						</fieldset>
					</form>

				</section>
				<hr>
				<section id="formularios-sem-rotulos">
					<h2>Formulários sem Rótulos</h2>

					<!-- Para copiar e colar -->
					<form action="#formularios-sem-rotulos" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>
							<div class="field-box nome">
								<input class="field" id="nome" maxlength="1055" name="nome" pattern="^[^0-9]{2,1055}$" placeholder="Nome *" required="required" title="Campo de texto obrigatório e de 2 a 1055 caracteres." type="text" />
							</div>
							<div class="field-box email">
								<input class="field" id="email" name="email" pattern="^[A-Za-z0-9\.-_@]{6,345}$" placeholder="E-mail *" required="required" type="email" title="Campo de e-mail obrigatório, alfanumérico, com ou sem caracteres especiais como &quot;.&quot;, &quot;-&quot; ou &quot;_&quot; e de 6 a 345 caracteres." />
							</div>
							<div class="field-box telefone_nacional">
								<input class="field" id="telefone_nacional" name="telefone_nacional" pattern="^\d{2} \d{4}\-\d{4}[0-9_]?$" placeholder="Telefone *" required="required" title="Campo de telefone obrigatório, somente com digítos, DDD e com o nono dígito opcional, como por exemplo: 99 9999-99999." type="tel" />
							</div>
							<div class="field-box mensagem">
								<textarea class="field autosize" id="mensagem" name="mensagem" placeholder="Mensagem *" required="required" title="Campo de texto obrigatório."></textarea>
							</div>
							<input type="submit" value="Enviar">
						</fieldset>
					</form>

				</section>
			</section>
			<section>
				<h1>Padrões de Campos</h1>
				<section id="campos-de-nomes-de-pessoas">
					<h2>Campos de Nomes de Pessoas</h2>
					<p><strong>Para usar agora:</strong></p>
					<p>O <code>pattern</code> e o <code>maxlength</code> permitem de 2 a 1055 caracteres, permitindo até o <a href="http://www.cmjornal.xl.pt/detalhe/noticias/ultima-hora/britanica-adopta-o-maior-nome-do-mundo" target="_blank">maior nome do mundo</a>.</p>
					<form action="#campos-de-nomes-de-pessoas" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box nome">
								<label for="nome">Nome *</label><br>
								<input class="field" id="nome" maxlength="1055" name="nome" pattern="^[^0-9]{2,1055}$" placeholder="Nome *" required="required" title="Campo de texto obrigatório e de 2 a 1055 caracteres." type="text" />
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
					<p><strong>Para evoluir antes de usar:</strong></p>
					<p>Este precisa ser evoluído para permitir a inserção de mais caracteres de outros idiomas.</p>

					<form action="#campos-de-nomes-de-pessoas" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box nome">
								<label for="nome">Nome *</label><br>
								<input class="field" id="nome" maxlength="1055" name="nome" pattern="^[A-Za-zÁÉÍÓÚÂÊÎÔÛÀÈÌÒÙÃÕÑÄËÏÖÜÇáéíóúâêîôûàèìòùãõñäëïöüç \.-]{2,1055}$" placeholder="Nome *" required="required" title="" type="text" />
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>

				</section>
				<hr />
				<section id="campos-de-emails">
					<h2>Campos de E-mails</h2>
					<ul>
						<li>O <code>pattern</code> e o <code>maxlength</code> permitem de 6 a 345 caracteres, permitindo até o <a href="http://tecnoblog.net/35664/australiano-quebra-recorde-mundial-de-maior-endereco-de-email/" target="_blank">maior e-mail do mundo</a></li>
						<li>O pattern também filtra para serem usados somente caracteres alfanuméricos não-acentuados, incluindo alguns caracteres especiais como &quot;<code>-</code>&quot;, &quot;<code>_</code>&quot; e &quot;<code>.</code>&quot;.</li>
					</ul>

					<form action="#campos-de-emails" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box email">
								<label for="email">E-mail *</label><br>
								<input class="field" id="email" name="email" pattern="^[A-Za-z0-9\.-_@]{6,345}$" placeholder="E-mail *" required="required" type="email" title="Campo de e-mail obrigatório, alfanumérico, com ou sem caracteres especiais como &quot;.&quot;, &quot;-&quot; ou &quot;_&quot; e de 6 a 345 caracteres." />
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>

				</section>
				<hr />
				<section id="campos-de-telefones">
					<h2>Campos de Telefones</h2>
					<section id="telefones-nacionais">
						<h3>Telefones Nacionais (com DDDs)</h3>
						<form action="#telefones-nacionais" class="validate" method="post">
							<fieldset>
								<legend>Exemplo de Formulário</legend>

								<!-- Para copiar e colar -->
								<div class="field-box telefone_nacional">
									<label for="telefone_nacional">Telefone *</label><br>
									<input class="field" id="telefone_nacional" name="telefone_nacional" pattern="^\d{2} \d{4}\-\d{4}[0-9_]?$" placeholder="Telefone *" required="required" title="Campo de telefone obrigatório, somente com digítos, DDD e com o nono dígito opcional, como por exemplo: 99 9999-99999." type="tel" />
								</div>

								<input type="submit" value="Enviar">
							</fieldset>
						</form>
					</section>
					<section id="telefones-internacionais">
						<h3>Telefones Internacionais (com DDIs e DDDs)</h3>
						<form action="#telefones-internacionais" class="validate" method="post">
							<fieldset>
								<legend>Exemplo de Formulário</legend>

								<!-- Para copiar e colar -->
								<div class="field-box telefone_internacional">
									<label for="telefone_internacional">Telefone *</label><br>
									<input class="field" id="telefone_internacional" name="telefone_internacional" pattern="^\+\d{2} \d{2} \d{4}\-\d{4}[0-9_]?$" placeholder="Telefone *" required="required" title="Campo de telefone obrigatório, somente com digítos, DDD e com o nono dígito opcional, como por exemplo: 99 9999-99999." type="tel" />
								</div>

								<input type="submit" value="Enviar">
							</fieldset>
						</form>
					</section>
					<section id="telefones-locais">
						<h3>Telefones Locais (sem DDDs)</h3>
						<form action="#telefones-locais" class="validate" method="post">
							<fieldset>
								<legend>Exemplo de Formulário</legend>

								<!-- Para copiar e colar -->
								<div class="field-box telefone_local">
									<label for="telefone_local">Telefone *</label><br>
									<input class="field" id="telefone_local" name="telefone_local" pattern="^\d{4}\-\d{4}[0-9_]?$" placeholder="Telefone *" required="required" title="Campo de telefone obrigatório, somente com digítos e com o nono dígito opcional, como por exemplo: 9999-99999." type="tel" />
								</div>

								<input type="submit" value="Enviar">
							</fieldset>
						</form>
					</section>
				</section>
				<hr />
				<section id="campos-de-cidades">
					<h2>Campos de Cidades</h2>
					<ul>
						<li>O <code>pattern</code> e o <code>maxlength</code> permitem de 2 a 176 caracteres, permitindo até o <a href="http://www.wambie.com/tuttifrutti_br/noticias/A_CIDADE_COM_O_NOME_MAIS_LONGO_DO_MUNDO-noticia_br-3778.html" target="_blank">mais comprido nome de cidade do mundo</a></li>
					</ul>
					<form action="#campos-de-cidades" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box cidade">
								<label for="cidade">Cidade *</label><br>
								<input class="field" id="cidade" maxlength="176" name="cidade" pattern="^{2,176}$" placeholder="Cidade *" required="required" title="Campo de texto obrigatório e de 2 a 176 caracteres." type="text" />
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
				<hr />
				<section id="campos-de-ceps">
					<h2>Campos de CEPs</h2>
					<ul>
						<li>Pattern visto <a href="http://tableless.com.br/html5/exemplos/8/pattern.html" target="_blank">aqui</a> e <a href="http://html5pattern.com/Postal_Codes" target="_blank">aqui</a> e adaptado para forçar o traço <code>-</code>. Somente dígitos e com <code>maxlength</code> de 9.</a></li>
					</ul>
					<form action="#campos-de-ceps" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box cep">
								<label for="cep">CEP *</label><br>
								<input class="field" id="cep" maxlength="9" name="cep" pattern="^\d{5}\-\d{3}$" placeholder="CEP *" required="required" title="Campo de texto obrigatório, CEP no formato 99999-999." type="text">
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
				<hr />
				<section id="campos-de-estados-e-ufs">
					<h2>Campos de Estados e UFs</h2>
					<section id="campos-de-estados">
						<h3>Campos de Estados</h3>
						<form action="#campos-de-estados" class="validate" method="post">
							<fieldset>
								<legend>Exemplo de Formulário</legend>

								<!-- Para copiar e colar -->
								<div class="field-box estado">
									<label for="estado">Estado *</label><br>
									<select class="field" id="estado" name="estado" required="required" title="Campo de seleção obrigatório.">
										<option disabled="disabled" selected="selected">Estado *</option>
										<option value="Acre">Acre</option>
										<option value="Alagoas">Alagoas</option>
										<option value="Amapá">Amapá</option>
										<option value="Amazonas">Amazonas</option>
										<option value="Bahia">Bahia</option>
										<option value="Ceará">Ceará</option>
										<option value="Distrito Federal">Distrito Federal</option>
										<option value="Espírito Santo">Espírito Santo</option>
										<option value="Goiás">Goiás</option>
										<option value="Maranhão">Maranhão</option>
										<option value="Mato Grosso">Mato Grosso</option>
										<option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
										<option value="Minas Gerais">Minas Gerais</option>
										<option value="Pará">Pará</option>
										<option value="Paraíba">Paraíba</option>
										<option value="Paraná">Paraná</option>
										<option value="Pernambuco">Pernambuco</option>
										<option value="Piauí">Piauí</option>
										<option value="Rio de Janeiro">Rio de Janeiro</option>
										<option value="Rio Grande do Norte">Rio Grande do Norte</option>
										<option value="Rio Grande do Sul">Rio Grande do Sul</option>
										<option value="Rondônia">Rondônia</option>
										<option value="Roraima">Roraima</option>
										<option value="Santa Catarina">Santa Catarina</option>
										<option value="São Paulo">São Paulo</option>
										<option value="Sergipe">Sergipe</option>
										<option value="Tocantins">Tocantins</option>
									</select>
								</div>

								<input type="submit" value="Enviar">
							</fieldset>
						</form>
					</section>
					<section id="campos-de-ufs">
						<h3>Campos de UFs</h3>
						<form action="#campos-de-ufs" class="validate" method="post">
							<fieldset>
								<legend>Exemplo de Formulário</legend>

								<!-- Para copiar e colar -->
								<div class="field-box uf">
									<label for="uf">UF *</label><br>
									<select class="field" id="uf" name="uf" required="required" title="Campo de seleção obrigatório.">
										<option disabled="disabled" selected="selected">UF *</option>
										<option value="AC" title="Acre">AC</option>
										<option value="AL" title="Alagoas">AL</option>
										<option value="AP" title="Amapá">AP</option>
										<option value="AM" title="Amazonas">AM</option>
										<option value="BA" title="Bahia">BA</option>
										<option value="CE" title="Ceará">CE</option>
										<option value="DF" title="Distrito Federal">DF</option>
										<option value="ES" title="Espírito Santo">ES</option>
										<option value="GO" title="Goiás">GO</option>
										<option value="MA" title="Maranhão">MA</option>
										<option value="MT" title="Mato Grosso">MT</option>
										<option value="MS" title="Mato Grosso do Sul">MS</option>
										<option value="MG" title="Minas Gerais">MG</option>
										<option value="PA" title="Pará">PA</option>
										<option value="PB" title="Paraíba">PB</option>
										<option value="PR" title="Paraná">PR</option>
										<option value="PE" title="Pernambuco">PE</option>
										<option value="PI" title="Piauí">PI</option>
										<option value="RJ" title="Rio de Janeiro">RJ</option>
										<option value="RN" title="Rio Grande do Norte">RN</option>
										<option value="RS" title="Rio Grande do Sul">RS</option>
										<option value="RO" title="Rondônia">RO</option>
										<option value="RR" title="Roraima">RR</option>
										<option value="SC" title="Santa Catarina">SC</option>
										<option value="SP" title="São Paulo">SP</option>
										<option value="SE" title="Sergipe">SE</option>
										<option value="TO" title="Tocantins">TO</option>
									</select>
								</div>

								<input type="submit" value="Enviar">
							</fieldset>
						</form>
					</section>
				</section>
				<hr />
				<section id="campos-de-mensagens">
					<h2>Campos de Mensagens</h2>
					<form action="#campos-de-mensagens" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box mensagem">
								<label for="mensagem">Mensagem *</label><br>
								<textarea class="field autosize" id="mensagem" name="mensagem" placeholder="Mensagem *" required="required" title="Campo de texto obrigatório."></textarea>
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
				<hr />
				<section id="campos-de-departamentos">
					<h2>Campos de Departamentos</h2>
					<form action="#campos-de-departamentos" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box departamento">
								<label for="departamento">Departamento *</label><br>
								<select class="field" id="departamento" name="departamento" required="required" title="Campo de seleção obrigatório.">
									<option disabled="disabled" selected="selected">Departamento *</option>
									<option value="1">Departamento 1</option>
									<option value="2">Departamento 2</option>
									<option value="3">Departamento 3</option>
								</select>
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
				<hr />
				<section id="campos-de-selects-customizados">
					<h2>Campos Select Customizados</h2>
					<form action="#campos-selects-customizados" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box select">
								<label for="select">Select *</label><br>
								<select class="field fancy-select" id="select" name="select" required="required" title="Campo de seleção obrigatório.">
									<option disabled="disabled" selected="selected" value="">Option *</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
				<hr />
				<section id="campos-de-anexos">
					<h2>Campos de Anexos</h2>
					<form action="#campos-de-anexos" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box anexo">
		  						<label for="anexo">Anexo *</label>
								<input class="field" id="anexo" name="anexo" required="required" title="Campo de envio de arquivo obrigatório." type="file" />
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
				<hr />
				<section id="campos-de-cpfs">
					<h2>Campos de CPFs</h2>
					<form action="#campos-de-cpfs" class="validate" method="post">
						<fieldset>
							<legend>Exemplo de Formulário</legend>

							<!-- Para copiar e colar -->
							<div class="field-box cpf">
								<label for="cpf">CPF *</label><br>
								<input class="field" id="cpf" name="cpf" pattern="^\d{3}\.\d{3}\.\d{3}\-\d{2}[0-9_]?$" required="required" title="Campo de CPF obrigatório, somente com digítos, como por exemplo: 999.999.999.99." type="text" />
							</div>

							<input type="submit" value="Enviar">
						</fieldset>
					</form>
				</section>
			</section>
		</main>
		<aside>
			<h1>Recursos e Referências</h1>
			<ul>
				<li><a href="//html5pattern.com/">html5pattern.com</a></li>
				<li><a href="//jquery.com/">jQuery</a></li>
				<li><a href="//jqueryvalidation.org/">jQuery Validation Plugin</a></li>
				<li><a href="//digitalbush.com/projects/masked-input-plugin/">Masked Input Plugin</a></li>
			</ul>
		</aside>

		<!-- jQuery -->
			<?php if ( ! IS_LOCALHOST) { ?>
				<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
			<?php } ?>
			<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>

		<!-- validate -->
			<?php if ( ! IS_LOCALHOST) { ?>
				<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
			<?php } ?>
			<script>window.jQuery.validator || document.write('<script src="js/vendor/jquery.validate.min.js"><\/script>')</script>
			<?php if ( ! IS_LOCALHOST) { ?>
				<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
			<?php } ?>
			<script>window.jQuery.validator.methods.pattern || document.write('<script src="js/vendor/additional-methods.min.js"><\/script>')</script>
			<script src="js/vendor/messages_pt_br.js"></script>

		<!-- maskedInput -->
			<?php if ( ! IS_LOCALHOST) { ?>
				<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js"></script>
			<?php } ?>
			<script>window.jQuery.mask || document.write('<script src="js/vendor/jquery.maskedinput.min.js"><\/script>')</script>

		<!-- autosize -->
			<?php if ( ! IS_LOCALHOST) { ?>
				<script src="//cdnjs.cloudflare.com/ajax/libs/autosize.js/1.18.1/jquery.autosize.min.js"></script>
			<?php } ?>
			<script>window.jQuery.fn.autosize || document.write('<script src="js/vendor/jquery.autosize.min.js"><\/script>')</script>

		<!-- fancyselect -->
			<script src="js/vendor/fancyselect.js"></script>

		<!-- this application -->
			<script src="js/main.js"></script>

	</body>
</html>
