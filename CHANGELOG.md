# Changelog

## 1.0.0 (3 de Março de 2014)

* Início da organização e documentação do projeto. (Gio)
* Inserido o Fancy Select - http://code.octopuscreative.com/fancyselect/ (Gio)

## 1.0.1 (4 de Março de 2014)

* Adicionadas regras mais específicadas para aplicações dos plugins no main.js e classes nos campos para ter mais flexibilidade de escolher quando quer aplicar o plugin ou não. Classe validate adicionada nos forms, classe autosize adicionada no textarea e plugins aplicados especificamente nos campos select, textarea e tags form. (Gio)