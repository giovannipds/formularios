$(function() {

	// põe a máscara nos telefones nacionais (com ddds)
	$('input[name="telefone_nacional"]').mask('99 9999-9999?9');

	// põe a máscara nos telefones internacionais (com ddis e ddds)
	$('input[name="telefone_internacional"]').mask('+99 99 9999-9999?9');

	// põe a máscara nos telefones locais (sem ddds)
	$('input[name="telefone_local"]').mask('9999-9999?9');

	// põe a máscara nos ceps
	$('input[name="cep"]').mask('99999-999');

	// põe a máscara nos cpfs
	$('input[name="cpf"]').mask('999.999.999-99');

	// autosize
	$('textarea.autosize').autosize({append: '\n'});

	// fancyselect
	$('select.fancy-select').fancySelect();

	// validates
	$('form.validate').validate();

	// apenas para as ações dessa página
	$('form.validate').submit(function(event) {
		event.preventDefault();
		if ($(this).valid()) {
			alert('Ok.');
		}
	});
	
});